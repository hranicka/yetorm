<?php

namespace Model\Repositories;

use Model\Entities\Book;
use Nette\Database\Context as NdbContext;
use Nette\Database\Table\ActiveRow as NActiveRow;
use YetORM;

/** @entity Book */
class BookRepository extends YetORM\Repository
{

	/** @var string */
	private $imageDir;

	/**
	 * @param  NdbContext $context
	 * @param  string $imageDir
	 */
	public function __construct(NdbContext $context, $imageDir)
	{
		parent::__construct($context);

		$realpath = realpath($imageDir);

		if ($realpath === FALSE || !is_dir($realpath)) {
			throw new \InvalidArgumentException;
		}

		$this->imageDir = $realpath;
	}

	/**
	 * @param YetORM\Entity|Book $book
	 * @return bool
	 */
	public function persist(YetORM\Entity $book)
	{
		$me = $this;

		return $this->transaction(function () use ($me, $book) {

			$status = parent::persist($book);

			// persist tags
			if (count($book->getAddedTags()) || count($book->getRemovedTags())) {
				$tags = $this->getTable('tag')->fetchPairs('name', 'id');
				foreach ($book->getAddedTags() as $tag) {
					if (!isset($tags[$tag->name])) {
						$tags[$tag->name] = $tagID = $this->getTable('tag')->insert([
							'name' => $tag->name,
						])->id;
					}

					$this->getTable('book_tag')->insert([
						'book_id' => $book->id,
						'tag_id' => $tags[$tag->name],
					]);
				}

				$toDelete = [];
				foreach ($book->getRemovedTags() as $tag) {
					if (isset($tags[$tag->name])) {
						$toDelete[] = $tags[$tag->name];
					}
				}

				if (count($toDelete)) {
					$this->getTable('book_tag')
						->where('book_id', $book->id)
						->where('tag_id', $toDelete)
						->delete();
				}
			}

			return $status;

		});
	}

	/** @return Book|NULL */
	public function getByID($id)
	{
		$row = $this->getTable()->get($id);
		return $row === FALSE ? NULL : $this->createBook($row);
	}

	/** @return YetORM\Collection */
	public function getByTag($name)
	{
		return $this->createCollection($this->getTable()->where(':book_tag.tag.name', $name), $this->createBook);
	}

	/** @return YetORM\Collection */
	public function getAll()
	{
		return $this->createCollection($this->getTable(), $this->createBook);
	}

	/**
	 * @param  NActiveRow $row
	 * @return Book
	 */
	public function createBook(NActiveRow $row = NULL)
	{
		return new Book($row, $this->imageDir);
	}

	/** @return string */
	public function getImageDir()
	{
		return $this->imageDir;
	}

}
