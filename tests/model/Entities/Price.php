<?php

namespace Model\Entities;

use YetORM\Entity;

/**
 * @property-read int $id
 * @property float $price
 * @property float $vat
 * @property float $total
 */
class Price extends Entity
{

}
