<?php

/**
 * This file is part of the YetORM library.
 *
 * Copyright (c) 2014 Jaroslav Hranička
 * Copyright (c) 2013, 2014 Petr Kessler (http://kesspess.1991.cz)
 *
 * @license  MIT
 * @link     https://bitbucket.org/hranicka/yetorm
 * @link     https://github.com/uestla/YetORM
 */

namespace YetORM\Reflection;

abstract class EntityProperty
{

	/** @var EntityType */
	private $reflection;

	/** @var string */
	private $name;

	/** @var bool */
	private $readOnly;

	/** @var string */
	private $type;

	/**
	 * @param EntityType $reflection
	 * @param string $name
	 * @param bool $readOnly
	 * @param string $type
	 */
	public function __construct(EntityType $reflection, $name, $readOnly, $type)
	{
		$this->reflection = $reflection;
		$this->name = (string)$name;
		$this->readOnly = (bool)$readOnly;
		$this->type = $this->getCommonType($type);
	}

	/**
	 * @return EntityType
	 */
	public function getEntityReflection()
	{
		return $this->reflection;
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @return bool
	 */
	public function isReadOnly()
	{
		return $this->readOnly;
	}

	/**
	 * @return string
	 */
	public function getType()
	{
		return $this->type;
	}

	/**
	 * @return bool
	 */
	public function isOfNativeType()
	{
		return self::isNativeType($this->type);
	}

	/**
	 * @param  string $type
	 * @return bool
	 */
	public static function isNativeType($type)
	{
		return ($type === NULL || $type === 'integer' || $type === 'float' || $type === 'double'
			|| $type === 'boolean' || $type === 'string' || $type === 'array');
	}

	/**
	 * @param string $value
	 * @return string
	 */
	private function getCommonType($value)
	{
		switch ($value) {
			case 'double':
			case 'float':
			case 'real':
				return 'double';
			case 'boolean':
			case 'bool':
				return 'boolean';
			case 'integer':
			case 'int':
				return 'integer';
			default:
				return (string)$value;
		}
	}

}
