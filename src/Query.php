<?php

namespace YetORM;

interface Query
{

	/**
	 * @param Repository $repository
	 * @return Collection|Entity[]
	 */
	public function run(Repository $repository);

}
