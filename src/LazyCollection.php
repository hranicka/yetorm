<?php

/**
 * Copyright © 2015 Jaroslav Hranička <hranicka@outlook.com>
 *
 * @license MIT
 * @link https://bitbucket.org/hranicka/yetorm
 */

namespace YetORM;

class LazyCollection implements Collection
{

	/** @var Collection */
	private $collection;

	/** @var int|null */
	private $limit;

	/** @var int|null */
	private $offset;

	/** @var int */
	private $batchLimit;

	/** @var int|null */
	private $batchOffset;

	/** @var Entity[] */
	private $stack;

	/** @var int */
	private $stackCounter;

	/** @var bool */
	private $done;

	/** @var Entity|null */
	private $currentEntity;

	/** @var int|null */
	private $currentKey;

	public function __construct(Collection $collection, $batchSize = 500)
	{
		$this->collection = $collection;
		$this->batchLimit = $batchSize;
		$this->reset();
	}

	public function current()
	{
		return $this->currentEntity;
	}

	public function key()
	{
		return $this->currentKey;
	}

	public function next()
	{
		if (!$this->stack) {
			$this->fetchNext();
		}

		if ($this->stack) {
			$this->currentEntity = array_shift($this->stack);
			$this->currentKey = ($this->currentKey === NULL) ? 0 : $this->currentKey + 1;
		} else {
			$this->currentEntity = NULL;
			$this->currentKey = NULL;
		}
	}

	public function valid()
	{
		return !!$this->current();
	}

	public function rewind()
	{
		$this->reset();
		$this->next();
	}

	/**
	 * @return array
	 */
	public function toArray()
	{
		return iterator_to_array($this);
	}

	/**
	 * @return $this
	 */
	public function resetOrder()
	{
		call_user_func_array([$this->collection, 'resetOrder'], func_get_args());
		return $this;
	}

	/**
	 * @param string|array $column
	 * @param bool $direction
	 * @return $this
	 */
	public function orderBy($column, $direction = NULL)
	{
		call_user_func_array([$this->collection, 'orderBy'], func_get_args());
		return $this;
	}

	/**
	 * @param int $limit
	 * @param int $offset
	 * @return $this
	 */
	public function limit($limit, $offset = NULL)
	{
		$this->limit = $limit;
		$this->offset = $offset;
		$this->reset();
		return $this;
	}

	/**
	 * @param $column
	 * @return int
	 */
	public function countDistinct($column)
	{
		return call_user_func_array([$this->collection, 'countDistinct'], func_get_args());
	}

	/**
	 * @return int
	 */
	public function count()
	{
		return call_user_func_array([$this->collection, 'count'], func_get_args());
	}

	private function reset()
	{
		$this->batchOffset = $this->offset;
		$this->stack = [];
		$this->stackCounter = 0;
		$this->currentEntity = NULL;
		$this->currentKey = NULL;
		$this->done = FALSE;
	}

	private function fetchNext()
	{
		if ($this->done) {
			return;
		}

		$limit = ($this->limit === NULL) ?
			$this->batchLimit :
			min($this->batchLimit, $this->limit - $this->stackCounter);

		// don't make request with LIMIT 0
		if ($limit) {
			$this->collection->limit($limit, $this->batchOffset);
			$this->stack = $this->collection->toArray();
		} else {
			$this->stack = [];
		}

		$this->stackCounter += count($this->stack);
		$this->batchOffset += $this->batchLimit;

		if (!$this->stack || count($this->stack) < $this->batchLimit) {
			$this->done = TRUE;
		}
	}

}
