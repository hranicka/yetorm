<?php

/**
 * Copyright © 2015 Jaroslav Hranička <hranicka@outlook.com>
 *
 * @license MIT
 * @link https://bitbucket.org/hranicka/yetorm
 */

namespace YetORM;

class FieldOrder
{

	/** @var string */
	private $field;

	/** @var array */
	private $values;

	public function __construct($field, array $values)
	{
		$this->field = $field;
		$this->values = $values;
	}

	/**
	 * @return string
	 */
	public function getField()
	{
		return $this->field;
	}

	/**
	 * @return array
	 */
	public function getValues()
	{
		return $this->values;
	}

}
