<?php

/**
 * This file is part of the YetORM library.
 *
 * Copyright (c) 2014 Jaroslav Hranička
 *
 * @license  MIT
 * @link     https://bitbucket.org/hranicka/yetorm
 */

namespace YetORM;

class EntitySleeper
{

	/** @var array */
	private $references = [];

	public function __construct(Entity $entity)
	{
		$this->addReference($entity);
	}

	/**
	 * @param EntityReference|null $reference
	 * @return array
	 */
	public function getValues(EntityReference $reference = NULL)
	{
		if ($reference) {
			$hash = $reference->getHash();
		} else {
			reset($this->references);
			$hash = key($this->references);
		}

		return $this->references[$hash];
	}

	/**
	 * @param EntityReference $entityReference
	 * @return Entity
	 */
	public function wakeUp(EntityReference $entityReference)
	{
		$ref = new \ReflectionClass($entityReference->getClass());

		/** @var Entity $entity */
		$entity = $ref->newInstanceWithoutConstructor();

		$entity->wakeUp($this, $entityReference);

		return $entity;
	}

	/**
	 * @param Entity $entity
	 * @return EntityReference
	 */
	private function addReference(Entity $entity)
	{
		$reference = new EntityReference($entity);

		if (!array_key_exists($reference->getHash(), $this->references)) {
			$values = $entity->toArray();

			$this->references[$reference->getHash()] = [
				'values' => & $values,
				'row' => $entity->toRow()->toArray(),
			];

			foreach ($values as & $value) {
				if ($value instanceof Entity) {
					$value = $this->addReference($value);
				}

				if (is_array($value)) {
					foreach ($value as & $subValue) {
						if ($subValue instanceof Entity) {
							$subValue = $this->addReference($subValue);
						}
					}
				}
			}
		}

		return $reference;
	}

}
